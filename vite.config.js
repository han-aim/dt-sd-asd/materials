import { defineConfig } from 'vite';
import { readFileSync } from 'fs';
import dns from 'dns';
import vitePugPlugin from 'vite-plugin-pug-transformer';

dns.setDefaultResultOrder('verbatim');
const optionsHttps = {
  ca: readFileSync('rootCA.pem'),
  cert: readFileSync('localhost.localdomain.pem'),
  key: readFileSync('localhost.localdomain-key.pem'),
};
const pages = ['index.html', 'ADP/index.html', 'SKA/index.html', 'OP/index.html', 'Docenten/index.html'];
export default defineConfig({
  base: '/dt-sd-asd/materials/',
  build: {
    emptyOutDir: true,
    manifest: true,
    outDir: 'dist',
    rollupOptions: {
      input: pages,
      output: {
        compact: true,
        generatedCode: 'es2015',
      },
      strictDeprecations: true,
    },
    sourcemap: true,
  },
  plugins: [vitePugPlugin()],
  server: { host: true, https: optionsHttps, strictPort: true },
});
