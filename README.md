<!-- LTeX: language=en -->
<!-- omit in toc -->
# Materials for the Advanced Software Development module

See [`lern/README.md`](lern/README.md).

## Configure Rancher Desktop for this application

Under macOS, set the following limits to at least these numbers:

`~/Library/Application Support/rancher-desktop/lima/_config/override.yaml`:

```yaml
---
provision:
  - mode: system
    script: |
      #!/bin/sh
      cat <<'EOF' > /etc/security/limits.d/rancher-desktop.conf
      * soft     nofile         82920
      * hard     nofile         82920
      EOF
```
