export const citationData = {
  items: [
    {
      id: '6867189/36G9SCWF',
      type: 'article',
      title: 'IEEE Reference Guide',
      publisher: 'IEEE Periodicals',
      URL: 'https://ieeeauthorcenter.ieee.org/wp-content/uploads/IEEE-Reference-Guide.pdf',
      language: 'en',
      issued: {
        'date-parts': [[2018]],
      },
    },
  ],
};
