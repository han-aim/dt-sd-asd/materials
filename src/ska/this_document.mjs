import { citationData } from './citationdata.mjs';
import { renderDocument } from '../render/document.mjs';

renderDocument(citationData);
