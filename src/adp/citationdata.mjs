export const citationData = {
  items: [
    {
      id: '6867189/VBGW22DZ',
      type: 'book',
      abstract:
        'Systems performance analysis and tuning lead to a better end-user experience and lower costs, especially for cloud computing environments that charge by the OS instance. Systems Performance, 2nd Edition covers concepts, strategy, tools, and tuning for operating systems and applications, using Linux-based operating systems as the primary example.World-renowned systems performance expert Brendan Gregg summarizes relevant operating system, hardware, and application theory to quickly get professionals up to speed even if they’ve never analyzed performance before, and to refresh and update advanced readers’ knowledge. Gregg illuminates the latest tools and techniques, including extended BPF, showing how to get the most out of your systems in cloud, web, and large-scale enterprise environments. He covers these and other key topics:*Hardware, kernel, and application internals, and how they perform*Methodologies for rapid performance analysis of complex systemsOptimizing CPU, memory, file system, disk, and networking usage*Sophisticated profiling and tracing with perf, Ftrace, and BPF (BCC and bpftrace)*Performance challenges associated with cloud computing hypervisors*Benchmarking more effectivelyFully updated for current Linux operating systems and environments, Systems Performance, 2nd Edition addresses issues that apply to any computer system. The book will be a go-to reference for many years to come and recommended reading at many tech companies, like its predecessor first edition.',
      edition: '2',
      'event-place': 'Boston',
      ISBN: '978-0-13-682015-4',
      language: 'English',
      'number-of-pages': '624',
      publisher: 'Pearson',
      'publisher-place': 'Boston',
      title: 'Systems Performance',
      author: [
        {
          family: 'Gregg',
          given: 'Brendan',
        },
      ],
      issued: {
        'date-parts': [['2020', 12, 16]],
      },
    },
    {
      id: '6867189/EW26IVKM',
      type: 'book',
      title: 'An introduction to the analysis of algorithms',
      publisher: 'Addison-Wesley',
      'number-of-pages': '52',
      edition: '2',
      ISBN: '978-0-321-90575-8',
      language: 'en',
      author: [
        {
          family: 'Flajolet',
          given: 'Philippe',
        },
        {
          family: 'Sedgewick',
          given: 'Robert',
        },
      ],
      issued: {
        'date-parts': [[2013]],
      },
    },
    {
      id: '6867189/GMIDVX6P',
      type: 'book',
      title: 'Introduction to Algorithms',
      publisher: 'The MIT Press',
      'publisher-place': 'Cambridge, Mass',
      'number-of-pages': '1292',
      edition: '3',
      'event-place': 'Cambridge, Mass',
      abstract:
        'The latest edition of the essential text and professional reference, with substantial new material on such topics as vEB trees, multithreaded algorithms, dynamic programming, and edge-based flow.Some books on algorithms are rigorous but incomplete; others cover masses of material but lack rigor. Introduction to Algorithms uniquely combines rigor and comprehensiveness. The book covers a broad range of algorithms in depth, yet makes their design and analysis accessible to all levels of readers. Each chapter is relatively self-contained and can be used as a unit of study. The algorithms are described in English and in a pseudocode designed to be readable by anyone who has done a little programming. The explanations have been kept elementary without sacrificing depth of coverage or mathematical rigor.The first edition became a widely used text in universities worldwide as well as the standard reference for professionals. The second edition featured new chapters on the role of algorithms, probabilistic analysis and randomized algorithms, and linear programming. The third edition has been revised and updated throughout. It includes two completely new chapters, on van Emde Boas trees and multithreaded algorithms, substantial additions to the chapter on recurrence (now called “Divide-and-Conquer”), and an appendix on matrices. It features improved treatment of dynamic programming and greedy algorithms and a new notion of edge-based flow in the material on flow networks. Many exercises and problems have been added for this edition. The international paperback edition is no longer available; the hardcover is available worldwide.',
      ISBN: '978-0-262-03384-8',
      language: 'English',
      author: [
        {
          family: 'Cormen',
          given: 'Thomas H.',
        },
        {
          family: 'Leiserson',
          given: 'Charles E.',
        },
        {
          family: 'Rivest',
          given: 'Ronald L.',
        },
        {
          family: 'Stein',
          given: 'Clifford',
        },
      ],
      issued: {
        'date-parts': [['2009', 7, 31]],
      },
    },
    {
      id: '6867189/L2768YLY',
      type: 'book',
      title: 'The art of computer programming',
      publisher: 'Addison-Wesley',
      'publisher-place': 'Reading, Mass',
      'number-of-pages': '3',
      edition: '3',
      'event-place': 'Reading, Mass',
      ISBN: '978-0-201-89683-1 978-0-201-89684-8 978-0-201-89685-5',
      'call-number': 'QA76.6 .K64 1997',
      author: [
        {
          family: 'Knuth',
          given: 'Donald Ervin',
        },
      ],
      issued: {
        'date-parts': [[1997]],
      },
    },
    {
      id: '6867189/PLHZEH2X',
      type: 'book',
      title: 'Algorithms',
      edition: '1',
      URL: 'http://jeffe.cs.illinois.edu/teaching/algorithms/',
      ISBN: '978-1-79264-483-2',
      language: 'en',
      author: [
        {
          family: 'Erickson',
          given: 'Jeff',
        },
      ],
      issued: {
        'date-parts': [['2019', 6]],
      },
    },
    {
      id: '6867189/LGMREFR6',
      type: 'article',
      title: 'The Java® Language Specification - Java SE 17 Edition',
      URL: 'https://docs.oracle.com/javase/specs/jls/se17/html/index.html',
      shortTitle: 'JLS',
      author: [
        {
          family: 'Gosling',
          given: 'James',
        },
        {
          family: 'Joy',
          given: 'Bill',
        },
        {
          family: 'Steele',
          given: 'Guy',
        },
        {
          family: 'Bracha',
          given: 'Gilad',
        },
        {
          family: 'Buckley',
          given: 'Alex',
        },
        {
          family: 'Smith',
          given: 'Daniel',
        },
        {
          family: 'Bierman',
          given: 'Gavin',
        },
      ],
    },
  ],
};
